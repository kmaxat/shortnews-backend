<?php namespace App\Services\Transformer;

use App\News;
use League\Fractal\TransformerAbstract;

class NewsTransformer extends TransformerAbstract
{

    public function transform(News $news)
    {
        return [
            'id'   => $news->id,
            'title' => $news->title,
            'description' => $news->description,
            'photo' => $news->photo != '' ? url('photo/' . $news->photo) : null,
            'views' => $news->views,
            'publish_at' => $news->publish_at,
            'draft_at' => $news->draft_at,
            'archive_at' => $news->archive_at,
            'created_at' => $news->created_at
        ];
    }
}