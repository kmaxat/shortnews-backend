<?php namespace App\Services\Form\News;

use App\Services\Validation\AbstractLaravelValidator;

class NewsUpdateFormValidator extends AbstractLaravelValidator
{
    protected $rules = [
        'id' => 'required|exists:news,id',
        'title' => 'required|max:255',
        'description' => 'required|max:255',
        'photo' => 'image|max:20148',
        'draft' => 'boolean'
    ];
}