<?php namespace App\Services\Form\News;

use App\Services\Validation\AbstractLaravelValidator;

class NewsCreateFormValidator extends AbstractLaravelValidator
{

    protected $rules = [
        'title' => 'required|max:255',
        'description' => 'required|max:255',
        'photo' => 'image|max:20148',
        'draft' => 'boolean'
    ];
}