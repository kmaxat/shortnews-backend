<?php namespace App\Services\Form;

abstract class AbstractForm
{
    protected $validators;

    protected $errors;

    public function registerValidator($name, $validator)
    {
        $this->validators[$name] = $validator;
    }

    public function isValid($name, array $data)
    {
        $this->beforeValidation();

        if ($this->validators[$name]->with($data)->passes()) {
            return true;
        }

        $this->errors = $this->validators[$name]->errors();
        return false;
    }

    /**
     * Return the errors
     *
     * @return \Illuminate\Support\MessageBag
     */
    public function errors()
    {
        return $this->errors;
    }

    protected function beforeValidation()
    {

    }
}