<?php namespace App\Services\Validation;

use Illuminate\Validation\Factory;

abstract class AbstractLaravelValidator implements ValidableInterface
{

    /**
     * Validator
     *
     * @var \Illuminate\Validation\Factory
     */
    protected $validator;

    /**
     * Validation data key => value array
     *
     * @var Array
     */
    protected $data = array();

    /**
     * Validation errors
     *
     * @var Array
     */
    protected $errors = array();

    /**
     * Validation rules
     *
     * @var Array
     */
    protected $rules = array();

    protected $messages = array();

    public function __construct(Factory $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Set data to validate
     *
     * @return \App\Services\Validation\AbstractLaravelValidator
     */
    public function with(array $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Replace placeholders with attributes
     *
     * @return array
     */
    public function replace()
    {
        $data = $this->data;
        $rules = $this->rules;

        array_walk($rules, function (&$rule) use ($data) {
            preg_match_all('/\{(.*?)\}/', $rule, $matches);

            foreach ($matches[0] as $key => $placeholder) {
                if (isset($data[$matches[1][$key]])) {
                    $rule = str_replace($placeholder, $data[$matches[1][$key]], $rule);
                }
            }
        });

        return $rules;
    }

    /**
     * Validation passes or fails
     *
     * @return Boolean
     */
    public function passes()
    {
        $rules = $this->replace();

        $validator = $this->validator->make($this->data, $rules, $this->messages);

        if ($validator->fails()) {
            $this->errors = $validator->messages();
            return false;
        }

        return true;
    }

    /**
     * Return errors, if any
     *
     * @return array
     */
    public function errors()
    {
        return $this->errors;
    }

}