<?php namespace App\Providers;

use App\Services\Form\News\NewsCreateFormValidator;
use App\Services\Form\News\NewsForm;
use App\Services\Form\News\NewsUpdateFormValidator;
use Illuminate\Support\ServiceProvider;

class FormServiceProvider extends ServiceProvider
{
    public function register()
    {
        $app = $this->app;

        $app->bind('App\Services\Form\News\NewsForm', function ($app) {
            $form = new NewsForm();
            $form->registerValidator('create', new NewsCreateFormValidator($app['validator']));
            $form->registerValidator('update', new NewsUpdateFormValidator($app['validator']));
            return $form;
        });
    }
}