<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class News extends Model
{
    use SoftDeletes;

    protected $table = 'news';

    protected $fillable = [
        'user_id',
        'title',
        'description',
        'views',
        'photo',
        'publish_at',
        'draft_at',
        'trash_at'
    ];

    public function getDates()
    {
        return [];
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function uploadPhoto(UploadedFile $photo)
    {

        $path = storage_path('news');
        $name = $this->id . '_' . str_random(5) . '.' . $photo->getClientOriginalExtension();

        $photo->move($path, $name);

        $this->photo = $name;
        $this->save();

        return true;
    }
}