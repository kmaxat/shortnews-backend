<?php namespace App\Http\Responses;

use Illuminate\Http\Response;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use UnexpectedValueException;

trait JsonOutput
{
    /**
     * Fractal library used for data presention and transformation.
     *
     * @var \League\Fractal\Manager
     */
    private $fractal;

    /**
     * Current status code of the given request.
     *
     * @var integer
     */
    protected $statusCode = Response::HTTP_OK;

    /**
     * Get the current status code.
     *
     * @return integer
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Set the current status code.
     *
     * @return JsonOutput
     */
    public function setStatusCode($code)
    {
        $this->statusCode = $code;

        return $this;
    }

    /**
     * Respond with a json array.
     *
     * @param  array $array
     * @param  array $headers
     * @return \Illuminate\Http\Response
     */
    protected function respondWithArray(array $array, array $headers = array())
    {
        return response()->json($array, $this->statusCode, $headers);
    }

    /**
     * Respond with an error.
     *
     * @param  stirng $message
     * @param  stirng $errorCode
     * @return \Illuminate\Http\Response
     */
    protected function respondWithError($message, $errorCode)
    {
        if ($this->statusCode == Response::HTTP_OK) {
            throw new UnexpectedValueException('Error response requested with 200 status code; user error.');
        }
        $out = $this->asError($message, $this->statusCode, $errorCode);
        return $this->respondWithArray($out);
    }

    /**
     * Respond with a single item.
     *
     * @param  mixed $item
     * @param  mixed $callback
     * @return \Illuminate\Http\Response
     */
    protected function respondWithItem($item, $callback)
    {
        $out = $this->asItemArray($item, $callback);
        return $this->respondWithArray($out);
    }

    /**
     * Respond with a collection of items.
     *
     * @param  array $collection
     * @param  mixed $callback
     * @return \Illuminate\Http\Response
     */
    protected function respondWithCollection($collection, $callback)
    {
        $out = $this->asCollectionArray($collection, $callback);

        return $this->respondWithArray($out);
    }

    public function asError($message, $statusCode, $errorCode)
    {
        return [
            'error' => [
                'code' => $errorCode,
                'http_code' => (int)$statusCode,
                'message' => $message,
            ]
        ];
    }

    /**
     * Output a single item.
     *
     * @param  mixed $item
     * @param  mixed $callback
     * @return array
     */
    public function asItemArray($item, $callback)
    {
        $resource = new Item($item, $callback);
        $root = $this->fractal->createData($resource);
        return $root->toArray();
    }

    /**
     * Output as a collection of items.
     *
     * @param  mixed $collection
     * @param  mixed $callback
     * @return array
     */
    public function asCollectionArray($collection, $callback)
    {
        $resource = new Collection($collection, $callback);
        $resource->setPaginator(new IlluminatePaginatorAdapter($collection));
        $root = $this->fractal->createData($resource);
        return $root->toArray();
    }

    public function errorNotFound($message = 'Not Found')
    {
        return response()->json(['status_code' => 404, 'message' => $message], 404);
    }
}