<?php namespace App\Http\Controllers;

use App\Http\Responses\JsonOutput;
use App\News;
use App\Services\Form\News\NewsForm;
use App\Services\Transformer\NewsTransformer;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use League\Fractal\Manager as Fractal;

class NewsController extends Controller
{
    use JsonOutput;

    public function __construct(Guard $auth,
                                News $news,
                                NewsForm $newsForm,
                                Fractal $fractal,
                                NewsTransformer $newsTransformer,
                                ImageManager $image)
    {
        $this->user = $auth->user();

        $this->news = $news;

        $this->fractal = $fractal;

        $this->newsForm = $newsForm;

        $this->newsTransformer = $newsTransformer;

        $this->image = $image;
    }

    public function index(Request $request, $status = null)
    {
        $news = $this->user->news();
        $per_page = $request->input('per_page');

        switch( $status ) {
            case 'draft':
                $news = $news->whereNotNull('draft_at');
                break;
            case 'trash':
                $news = $news->whereNotNull('trash_at');
                break;
            case 'archive':
                $news = $news->whereNotNull('archive_at');
                break;
            default:
                $news = $news->whereNotNull('publish_at');
                break;
        }

        $news = $news->paginate( $per_page );

        return $this->respondWithCollection($news, $this->newsTransformer);
    }

    public function store(Request $request)
    {

        if( !$this->newsForm->isValid('create', $request->all()) ) {
            return response()->json(['errors' => $this->newsForm->errors()]);
        }

        $news = $this->news->create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'user_id' => $this->user->id,
            'publish_at' => !$request->input('draft') ? new Carbon() : null,
            'draft_at' => $request->input('draft') ? new Carbon() : null,
        ]);

        if( $request->hasFile('photo') ) {
            $news->uploadPhoto($request->file('photo'));
        }

        return $this->respondWithItem($news, $this->newsTransformer);
    }

    public function update(Request $request, $id)
    {
        $request->merge(['id' => $id]);

        if( !$this->newsForm->isValid('update', $request->all()) ) {
            return response()->json(['errors' => $this->newsForm->errors()]);
        }

        $news = $this->news->find($id);

        $news->fill([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'publish_at' => !$request->input('draft') ? new Carbon() : null,
            'draft_at' => $request->input('draft') ? new Carbon() : null,
        ]);
        $news->save();

        if( $request->hasFile('photo') ) {
            $news->uploadPhoto($request->file('photo'));
        }

        return $this->respondWithItem($news, $this->newsTransformer);
    }

    public function show($id)
    {
        $news = $this->user->news()->find($id);

        if( !$news ) {
            return $this->errorNotFound();
        }

        return $this->respondWithItem($news, $this->newsTransformer);
    }

    public function photo($name)
    {
        $path = storage_path('news') . '/' . $name;

        $image = $this->image->make($path)->fit(160, 120);

        return $image->response();
    }

    public function archive($id)
    {
        $news = $this->user
            ->news()
            ->where('id', $id)
            ->where(function($sql){
                $sql->orWhereNotNull('archive_at')
                    ->orWhereNotNull('trash_at');
            })
            ->first();

        if( !$news ) {
            return $this->errorNotFound();
        }

        $news->archive_at = new Carbon();
        $news->save();

        return response()->json('ok');
    }

    public function restore($id)
    {
        $news = $this->user
            ->news()
            ->where('id', $id)
            ->where(function($sql){
                $sql->orWhereNotNull('archive_at')
                    ->orWhereNotNull('trash_at');
            })
            ->first();

        if( !$news ) {
            return $this->errorNotFound();
        }

        $news->fill([
            'trash_at' => null,
            'archive_at' => null,
            'publish_at' => new Carbon()
        ]);
        $news->save();

        return response()->json('ok');
    }

    public function trash($id)
    {
        $news = $this->user
            ->news()
            ->where('id', $id)
            ->whereNull('trash_at')
            ->first();

        if( !$news ) {
            return $this->errorNotFound();
        }

        $news->fill([
            'trash_at' => new Carbon(),
            'publish_at' => null,
        ]);
        $news->save();

        return response()->json('ok');
    }
}