<?php

$router->post('login', function () {
    $credentials = Input::only('email', 'password');
    try {
        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'invalid_credentials'], 401);
        }
    } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
        return response()->json(['error' => 'could_not_create_token'], 500);
    }
    return response()->json(compact('token'));
});

$router->post('register', function(){
    $validator = Validator::make(Request::only('email', 'password'), [
        'email' => 'required|email|unique:users,email',
        'password' => 'required|min:5'
    ]);
    if( $validator->fails() ) {
        return response()->json(['errors' => $validator->errors()], 400);
    }
    $user = \App\User::create([
        'email' => Request::input('email'),
        'password' => Request::input('password')
    ]);
    return response()->json($user);
});

$router->get('photo/{name}', 'NewsController@photo');

$router->group(['prefix' => 'v1', 'middleware' => 'jwt.auth'], function () use ($router) {
    $router->get('news',        'NewsController@index');
    $router->get('news/draft',  'NewsController@index');
    $router->get('news/archive', 'NewsController@index');
    $router->get('news/trash',  'NewsController@index');
    $router->get('news/{id}',   'NewsController@show');

    $router->post('news',       'NewsController@store');
    $router->patch('news/{id}', 'NewsController@update');

    $router->post('news/{id}/archive',  'NewsController@archive');
    $router->post('news/{id}/restore',  'NewsController@restore');
    $router->post('news/{id}/trash',    'NewsController@trash');
});