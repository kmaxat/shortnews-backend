## Installation

> git clone git@bitbucket.org:kmaxat/shortnews-backend.git

> composer install

> php artisan migrate

> php artisan db:seed

## Update

> git pull

> php artisan migrate