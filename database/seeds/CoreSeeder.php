<?php

class CoreSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $this->users();
    }

    protected function users()
    {
        DB::table('users')->truncate();
        \App\User::create([
            'name' => 'user',
            'email' => 'user@example.com',
            'password' => bcrypt('12345')
        ]);
    }
}