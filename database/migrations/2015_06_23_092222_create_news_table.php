<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function(Blueprint $t){
            $t->increments('id');
            $t->integer('user_id');
            $t->string('title');
            $t->string('description');
            $t->integer('views');
            $t->string('photo');
            $t->timestamp('publish_at')->nullable();
            $t->timestamp('draft_at')->nullable();
            $t->timestamp('archive_at')->nullable();
            $t->timestamp('trash_at')->nullable();
            $t->timestamps();
            $t->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
